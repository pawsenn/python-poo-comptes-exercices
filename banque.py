"""
contient la classe Compte et ses classes enfants CompteCourant et CompteEpargne
"""


class Compte:
    def __init__(self, numero_compte: int, nom_proprietaire: str, solde=0.0):
        """
        compte bancaire générique
        :param numero_compte: le numéro du compte
        :param nom_proprietaire: le nom du propriétaire du compte
        :param solde: le solde du compte
        """
        self.__numero_compte = numero_compte
        self.__nom_proprietaire = nom_proprietaire
        self.__solde = solde

    @property
    def numero_compte(self):
        return self.__numero_compte

    @property
    def nom_proprietaire(self):
        return self.__nom_proprietaire

    @property
    def solde(self):
        return self.__solde

    @numero_compte.setter
    def numero_compte(self, value):
        self.__numero_compte = value

    @nom_proprietaire.setter
    def nom_proprietaire(self, value):
        self.__nom_proprietaire = value

    @solde.setter
    def solde(self, value):
        self.__solde = value

    def retrait(self, debit: float):
        """
        débite le compte
        :param debit: somme à soustraire du compte
        :return: None
        """
        self.solde = self.solde - debit

    def versement(self, credit: float):
        """
        crédite le compte
        :param credit: somme à ajouter au compte
        :return: None
        """
        self.solde = self.solde + credit

    def afficher_solde(self):
        """
        affiche le solde
        :return: None
        """
        print(self.solde)


class CompteCourant(Compte):
    def __init__(self, numero_compte, nom_proprietaire, solde=0, autorisation_decouvert=500.0, pourcentage_agios=0.05):
        """
        compte bancaire courant
        :param numero_compte: le numéro du compte
        :param nom_proprietaire: le nom du propriétaire du compte
        :param solde: le solde du compte
        :param autorisation_decouvert: seuil de solde négatif pour lequel il n'y a pas de pénalité
        :param pourcentage_agios: pourcentage de frais lorsque le comopte est en dessous de l'autorisation de découvert
        """
        super().__init__(numero_compte, nom_proprietaire, solde)
        self.__autorisation_decouvert = autorisation_decouvert
        self.__pourcentage_agios = pourcentage_agios

    @property
    def autorisation_decouvert(self) -> float:
        return self.__autorisation_decouvert

    @property
    def pourcentage_agios(self) -> float:
        return self.__pourcentage_agios

    @autorisation_decouvert.setter
    def autorisation_decouvert(self, value):
        self.__autorisation_decouvert = value

    @pourcentage_agios.setter
    def pourcentage_agios(self, value):
        self.__pourcentage_agios = value

    def appliquer_agios(self):
        """
        applique un agio au compte courant
        :return: None
        """
        if self.solde < -self.autorisation_decouvert:
            self.solde = self.solde - abs(self.solde) * self.pourcentage_agios


class CompteEpargne(Compte):
    def __init__(self, numero_compte, nom_proprietaire, solde=0, pourcentage_interets=0.05):
        """
        compte bancaire d'épargne
        :param numero_compte: le numéro du compte
        :param nom_proprietaire: le nom du propriétaire du compte
        :param solde: le solde du compte
        :param pourcentage_interets: le pourcetage d'intérets du comptes
        """
        super().__init__(numero_compte, nom_proprietaire, solde)
        self.__pourcentage_interets = pourcentage_interets

    @property
    def pourcentage_interets(self) -> float:
        return self.__pourcentage_interets

    @pourcentage_interets.setter
    def pourcentage_interets(self, value):
        self.__pourcentage_interets = value

    def appliquer_interet(self):
        """
        applique un interet au compte epargne
        :return: None
        """
        self.solde = self.solde * (1 + self.pourcentage_interets)

