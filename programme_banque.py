"""
programme qui effectue des opérations sur un sélectionné
"""

import banque
import csv


def demander_nom() -> str:
    """
    demande un nom via un input
    :return: le nom entré
    """
    nom = input("\nPropriétaire du compte: \n")
    return nom


def demander_type_compte() -> str:
    """
    demande le type de compte voulu via un input
    :return: "1" si compte courant, "2" si compte épargne
    """
    while True:
        select_type_compte = input("\nTapez '1' pour selectionner le compte courant\n"
                                   "Tapez '2' pour selectionner le compte épargne :\n")
        if select_type_compte in ("1", "2"):
            return select_type_compte
        else:
            print("\nErreur : mauvaise entrée, essayez à nouveau\n")


def demander_montant_operation() -> float:
    """
    demande le montant de l'opération a effectué sur le solde du compte via un input
    :return: None
    """
    while True:
        try:
            select_montant = input("\nEntrez le montant à débiter (valeur négative)\n"
                                   "ou le montant à créditer (valeur positive) :\n")
            return float(select_montant)
        except ValueError:
            print("\nErreur : mauvaise entrée, essayez à nouveau\n")
            continue


def csv_to_list(filename: str) -> list:
    """
    convertit un fichier csv en liste (une ligne du csv -> un item de la liste)
    :param filename: le nom du fichier csv
    :return: une liste contenant les lignes du fichier csv
    """
    with open(filename, "r", newline="") as f:
        reader = csv.reader(f, delimiter=";")
        list_file = list(reader)
    return list_file


def list_to_csv(list_for_file: list, filename) -> None:
    """
    convertir une liste en fichier csv
    :param list_for_file: liste des éléments à écrire dans le fichier csv
    :param filename: le nom du fichier à modifier
    :return: None
    """
    with open(filename, "w", newline="") as f:
        writer = csv.writer(f, delimiter=";")
        writer.writerows(list_for_file)


def appli_banque(csv_file: str):
    """
    programme principal qui effectue des opérations sur un compte sélectionné
    :csv_file: fichier csv contenant les comptes sous forme nom_utilisateur;numero_compte;type_compte;solde
    :return: None
    """
    liste_comptes_csv = csv_to_list(csv_file)  # ["Bob";111;"courant";-300]
    comptes = []
    # liste de propriétés vers liste de comptes
    for compte in liste_comptes_csv:
        if compte[2] == "courant":
            new_compte = banque.CompteCourant(int(compte[1]), compte[0], float(compte[3]))
        else:
            new_compte = banque.CompteEpargne(int(compte[1]), compte[0], float(compte[3]))
        comptes.append(new_compte)

    mon_compte = None
    message = ""
    while True:
        nom = demander_nom()
        type_compte = demander_type_compte()
        # teste si les infos entrées par l'utilisateur correspondent à un compte de la liste de comptes
        n = 0
        for compte in comptes:
            if n == len(comptes):
                print("\nErreur : mauvaise entrée, essayez à nouveau\n")
            if (nom, type_compte) == (compte.nom_proprietaire,
                                      "1" if isinstance(compte, banque.CompteCourant) else "2"):
                mon_compte = compte
                if type_compte == "1":
                    message = f"Votre autorisation de découvert est de {mon_compte.autorisation_decouvert}" \
                              f" euros\n" \
                              f"Le pourcentage d'agios est de {mon_compte.pourcentage_agios * 100}%"
                elif type_compte == "2":
                    message = f"Le taux d'intérêt est {mon_compte.pourcentage_interets*100}%"
                break
            else:
                n += 1
        else:
            print("\n Erreur : la combinaison propriétaire/type de compte n'existe pas."
                  " Veuillez réessayer.")
            continue
        break

    # opérations sur le compte sélectionné
    while True:
        montant = demander_montant_operation()
        print(f"\nAncien solde: {mon_compte.solde} euros")
        if montant >= 0:
            mon_compte.versement(montant)
            operation = f"versement de {montant} euros\n"
        else:
            mon_compte.retrait(abs(montant))
            operation = f"retrait de {abs(montant)} euros\n"
        if type_compte == "1":
            mon_compte.appliquer_agios()
        else:
            mon_compte.appliquer_interet()
        print(f"Opération effectuée : {operation}"
              f"{message}\n"
              f"Nouveau solde: {mon_compte.solde} euros")

        select_again = input("\nVoulez-vous effectuer une autre opération sur votre compte ? (o/n):\n")
        if select_again in ("o", "n"):
            if select_again == "o":
                continue
            else:
                print("\nAu revoir !\n")
                # liste de comptes vers liste de propriétés
                liste_compte_fin = []
                for compte in comptes:
                    liste_compte_fin.append([
                        compte.nom_proprietaire,
                        compte.numero_compte,
                        "courant" if isinstance(compte, banque.CompteCourant) else "epargne",
                        compte.solde
                    ])
                list_to_csv(liste_compte_fin, csv_file)
                break
        else:
            print("\nErreur : mauvaise entrée, essayez à nouveau\n")


if __name__ == "__main__":
    appli_banque("liste_comptes.csv")

